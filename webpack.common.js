const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: {
    main: './src/index.js'
  },
  output: {
    filename: '[name].bundle.js',
    // chunkFilename: '[name].bundle.js',
    publicPath: '/',
    path: resolve(__dirname, 'dist')
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Website',
      template: 'src/index.ejs',
      scriptLoading: 'defer',
      inject: 'body',
      cache: true,
      showErrors: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [[
              '@babel/preset-env',
              {
                targets: { browsers: ['last 2 Chrome versions', 'Safari 11'] }
              }
            ]],
            plugins: [
              [
                '@babel/plugin-proposal-decorators',
                {
                  decoratorsBeforeExport: true
                }
              ],
              '@babel/plugin-proposal-class-properties',
              // [
              //   '@babel/plugin-transform-runtime',
              //   {
              //     helpers: false,
              //     regenerator: true
              //   }
              // ],
              [
                '@babel/plugin-proposal-object-rest-spread',
                { useBuiltIns: true }
              ]
            ]
          }
        }
      },
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.worker\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          { loader: 'worker-loader' },
          {
            loader: 'babel-loader',
            options: {
              presets: [[
                '@babel/preset-env',
                {
                  targets: { browsers: ['last 2 Chrome versions', 'Safari 11'] }
                }
              ]],
              plugins: [
                [
                  '@babel/plugin-proposal-decorators',
                  {
                    decoratorsBeforeExport: true
                  }
                ],
                '@babel/plugin-proposal-class-properties',
                // [
                //   '@babel/plugin-transform-runtime',
                //   {
                //     helpers: false,
                //     regenerator: true
                //   }
                // ],
                [
                  '@babel/plugin-proposal-object-rest-spread',
                  { useBuiltIns: true }
                ]
              ]
            }
          }
        ]
      }
    ]
  }
};
