const { resolve } = require('path');
const { merge } = require('webpack-merge');
const ManifestPlugin = require('webpack-manifest-plugin');
const common = require('./webpack.common');
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new ManifestPlugin(),
    new WorkboxPlugin.InjectManifest({
      swSrc: resolve(__dirname, './src/service-worker/index.js'),
      swDest: 'sw.js'
    })
  ]
});
